# JIM Python Server
To get started running the JIM server run these commands after cloning:

	python server.py
	
NOTE: that the JIM Python Server requiers python 2.5 or greater but is incompatable at the moment with 3.x.
Certain linux distributions may rename the 2.x binary as python2 in which case the command would be
	
	python2 server.py