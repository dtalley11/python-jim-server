import socket
import thread
import time
import json

# Define these variables early 
HOST = ""
PORT = 4004
usernames=[]

# Functions
def accept(conn):
    def threaded():
        conn.send("{response: 401}") # Once connected request authentication
        while True: # Setting the user up with a nickname
            try:
                name = conn.recv(16384).strip()
            except socket.error:
                continue
            if name in users: # if the username was a duplicate, send a 409 to let the client know about this
                conn.send("{response: 409}") 
            elif name:
                conn.send("{response: 200}")
                usernames.append(name)
                conn.setblocking(False)
                users[name] = conn
                json_server_message("msg", "welcome, "+name+" There are "+str(len(usernames))+" users on", name)
                json_server_message("msg", ', '.join(usernames)+"are currently on", name)
                broadcast(name, "+++ %s arrived +++" % name)
                break
    thread.start_new_thread(threaded, ())
   
def json_server_message(type, input, name): # Encode message into JSON
    message=("{action: '"+str(type)+"', message: '"+str(input)+"'}") #the basic idea here is {action: 'msg', message: 'message'}
    print(message)
    for to_name, conn in users.items(): # This makes sure the message gets to the right person
        if to_name == name:
            conn.send(message)
    
def broadcast(name, message): # Send messages of one user to everybody else
    print message # For logging
    for to_name, conn in users.items():
        if to_name != name:
            try:
                json_client_send(to_name, "msg", message)
            except socket.error:
                pass

def json_client_send(name, action, message): # Send Message to user
    print("client send: {action: '"+action+"', nick: '"+name+"', message: '" + message + "',}")
    conn.send("{action: '"+action+"', nick: '"+name+"', message: '" + message + "',}")
 
# Set up the server socket.
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.setblocking(False)
s.bind((HOST, PORT))
s.listen(1)
print "Listening on %s" % ("%s:%s" % s.getsockname())
 
# Main event loop.
users = {}
while True:
    try:
        # Accept new connections.
        while True:
            try:
                conn, addr = s.accept()
            except socket.error:
                break
            accept(conn)
        # Read from connections.
        for name, conn in users.items():
            try:
                message = conn.recv(1024)
            except socket.error:
                continue
            if not message:
                # Empty string is given on disconnect.
                del users[name]
                usernames = [i for i in usernames if i != name]
                broadcast(name, "--- %s leaves ---" % name)
            else:
                broadcast(name, "%s %s" % (name, message.strip()))
        time.sleep(.1)
    except (SystemExit, KeyboardInterrupt):
        break